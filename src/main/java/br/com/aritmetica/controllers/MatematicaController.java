package br.com.aritmetica.controllers;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import br.com.aritmetica.services.MatematicaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.lang.reflect.MalformedParameterizedTypeException;


@RestController
@RequestMapping("/matematica")
public class MatematicaController {

    @Autowired
    private MatematicaService matematicaservice;

    @PutMapping("/soma")
    public RespostaDTO soma(@RequestBody EntradaDTO entradaDTO)
    {
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros para serem somados");
        }
        RespostaDTO resposta = matematicaservice.soma(entradaDTO);
        return resposta;
    }

    @PutMapping("/subtrai")
    public RespostaDTO subtrai(@RequestBody EntradaDTO entradaDTO)
    {
        if(entradaDTO.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Envie pelo menos 2 numeros para serem subtraidos");
        }
        RespostaDTO resposta = matematicaservice.subtrai(entradaDTO);
        return resposta;
    }
}
