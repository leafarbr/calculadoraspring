package br.com.aritmetica.services;

import br.com.aritmetica.DTOs.EntradaDTO;
import br.com.aritmetica.DTOs.RespostaDTO;
import org.springframework.stereotype.Service;

@Service
public class MatematicaService {

    public RespostaDTO soma(EntradaDTO entradaDTO)
    {

        int numero;
        numero = 0;
        for (int i : entradaDTO.getNumeros())
        {
            numero += i;
        }

        RespostaDTO resultadoDTO = new RespostaDTO();
        resultadoDTO.setResultado(numero);

        return resultadoDTO;


    }

    public RespostaDTO subtrai(EntradaDTO entradaDTO)
    {

        int numero;
        numero = 0;
        for (int i : entradaDTO.getNumeros())
        {
            numero -= i;
        }

        RespostaDTO resultadoDTO = new RespostaDTO();
        resultadoDTO.setResultado(numero);

        return resultadoDTO;


    }

}
